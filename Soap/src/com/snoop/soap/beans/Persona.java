package com.snoop.soap.beans;

import java.io.Serializable;

public class Persona  implements Serializable{
	private static final long serialVersionUID = -5577579081118070434L;
	private int id;
	private int edad;
	String nombre;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	@Override
	public String toString(){
		return id+"::"+nombre+"::"+edad;
	}
}
