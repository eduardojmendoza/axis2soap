package com.snoop.soap.servicio;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import com.snoop.soap.beans.Persona;

public class PersonaServiceImpl implements PersonaService{
	
	private static Map<Integer,Persona> personas = new HashMap<Integer,Persona>();
	
	@Override
	public boolean addPersona(Persona p) {
		if(personas.get(p.getId()) != null) return false;
		personas.put(p.getId(), p);
		return true;
	}

	@Override
	public boolean deletePersona(int id) {
		if(personas.get(id) == null) return false;
		personas.remove(id);
		return true;
	}

	@Override
	public Persona getPersona(int id) {
		return personas.get(id);
	}

	@Override
	public Persona[] getAllPersonas() {
		Set<Integer> ids = personas.keySet();
		Persona[] p = new Persona[ids.size()];
		int i=0;
		for(Integer id : ids){
			p[i] = personas.get(id);
			i++;
		}
		return p;
	}
	
	
	@Test
	public void pruebaMock() {
		//esto seria el get persona
	
		
		System.out.println("Resultado ");
	}

}
