package com.snoop.soap.servicio;

import com.snoop.soap.beans.Persona;

public interface PersonaService {
	public boolean addPersona(Persona p);
	public boolean deletePersona(int id);
	public Persona getPersona(int id);
	public Persona[] getAllPersonas();
}
